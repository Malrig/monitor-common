# Internet Monitor Common Code

This is a small library used by my internet monitor Python app. It provides the MongoDB integration as well as globally
used classes and enums.

## Installation

To install from PyPi run:
```bash
pip install monitor-common
```

## Development and deployment

See the [Contribution guidelines for this project](CONTRIBUTING.md) for details on how to make changes to this library.
