import pytest

from monitor_common.status import MyEnum, MyOrderedEnum


class EnumToTest(MyEnum):
    ALL_CAPS = 0
    no_caps = 1
    SoMe_CaPs = 2


class TestMyEnum:
    @pytest.mark.parametrize(
        "input_string, expected_value, expected_name",
        [
            ("ALL_CAPS", 0, "ALL_CAPS"),
            ("all_caps", 0, "ALL_CAPS"),
            ("no_caps", 1, "no_caps"),
            ("NO_CAPS", 1, "no_caps"),
            ("SoMe_CaPs", 2, "SoMe_CaPs"),
            ("sOmE_cApS", 2, "SoMe_CaPs"),
        ],
    )
    def test_from_string(self, input_string, expected_value, expected_name):
        new_enum = EnumToTest[input_string]

        assert new_enum.value == expected_value
        assert new_enum.name == expected_name

    @pytest.mark.parametrize(
        "input_index, expected_value, expected_name",
        [(0, 0, "ALL_CAPS"), (1, 1, "no_caps"), (2, 2, "SoMe_CaPs")],
    )
    def test_from_index(self, input_index, expected_value, expected_name):
        new_enum = EnumToTest[input_index]

        assert new_enum.value == expected_value
        assert new_enum.name == expected_name


class OrderedEnumToTest(MyOrderedEnum):
    ALL_CAPS = 0
    no_caps = 1
    SoMe_CaPs = 2


class OtherOrderedEnum(MyOrderedEnum):
    FIRST_ENTRY = 2
    SECOND_ENTRY = 4


class TestMyOrderedEnum:
    @pytest.mark.parametrize(
        "input_string, expected_value, expected_name",
        [
            ("ALL_CAPS", 0, "ALL_CAPS"),
            ("all_caps", 0, "ALL_CAPS"),
            ("no_caps", 1, "no_caps"),
            ("NO_CAPS", 1, "no_caps"),
            ("SoMe_CaPs", 2, "SoMe_CaPs"),
            ("sOmE_cApS", 2, "SoMe_CaPs"),
        ],
    )
    def test_from_string(self, input_string, expected_value, expected_name):
        new_enum = OrderedEnumToTest[input_string]

        assert new_enum.value == expected_value
        assert new_enum.name == expected_name

    @pytest.mark.parametrize(
        "input_index, expected_value, expected_name",
        [(0, 0, "ALL_CAPS"), (1, 1, "no_caps"), (2, 2, "SoMe_CaPs")],
    )
    def test_from_index(self, input_index, expected_value, expected_name):
        new_enum = OrderedEnumToTest[input_index]

        assert new_enum.value == expected_value
        assert new_enum.name == expected_name

    def test_comparisons(self):
        assert OrderedEnumToTest.no_caps == OrderedEnumToTest.no_caps
        assert OrderedEnumToTest.no_caps <= OrderedEnumToTest.no_caps
        assert OrderedEnumToTest.no_caps >= OrderedEnumToTest.no_caps
        assert OrderedEnumToTest.no_caps > OrderedEnumToTest.ALL_CAPS
        assert OrderedEnumToTest.no_caps >= OrderedEnumToTest.ALL_CAPS
        assert OrderedEnumToTest.no_caps < OrderedEnumToTest.SoMe_CaPs
        assert OrderedEnumToTest.no_caps <= OrderedEnumToTest.SoMe_CaPs

    def test_cannot_compare_different_ordered_enums(self):
        assert OrderedEnumToTest.SoMe_CaPs != OtherOrderedEnum.FIRST_ENTRY
        with pytest.raises(TypeError):
            OrderedEnumToTest.no_caps >= OtherOrderedEnum.FIRST_ENTRY
        with pytest.raises(TypeError):
            OrderedEnumToTest.no_caps <= OtherOrderedEnum.FIRST_ENTRY
        with pytest.raises(TypeError):
            OrderedEnumToTest.no_caps < OtherOrderedEnum.FIRST_ENTRY
        with pytest.raises(TypeError):
            OrderedEnumToTest.no_caps > OtherOrderedEnum.FIRST_ENTRY
