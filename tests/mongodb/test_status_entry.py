import pytest

from datetime import datetime

from monitor_common.mongodb.status_entry import StatusEntry
from monitor_common.status import Status, StatusChange


class TestStatusDoc:
    def test_initialisation(self):
        entry = StatusEntry(
            time=datetime(2018, 9, 24),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.ERROR_RESOLVED,
        )

        entry.validate()
        assert entry.time == datetime(2018, 9, 24)
        assert entry.result
        assert entry.status == Status.WARNING
        assert entry.status_change == StatusChange.ERROR_RESOLVED == entry.status_change

    def test_save_retrieve(self, mocked_db_clean):
        entry = StatusEntry(
            time=datetime(2018, 9, 24),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.ERROR_RESOLVED,
        )
        entry.save()

        entries = StatusEntry.objects()
        saved_entry = entries.first()

        assert entries.count() == 1
        assert saved_entry == entry

    def test_set_properties(self):
        entry = StatusEntry()
        entry.time = datetime(2018, 9, 24)
        entry.result = True
        entry.status = Status.WARNING
        entry.status_change = StatusChange.ERROR_RESOLVED

        assert entry.time == datetime(2018, 9, 24)
        assert entry.result
        assert entry.status == Status.WARNING
        assert entry.status_change == StatusChange.ERROR_RESOLVED == entry.status_change

    def test_to_dict(self):
        entry = StatusEntry(
            time=datetime(2018, 9, 24),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.ERROR_RESOLVED,
        )

        expected_dict = {
            "time": str(datetime(2018, 9, 24)),
            "result": True,
            "status": "WARNING",
            "status_change": "ERROR_RESOLVED",
        }

        assert entry.to_dict() == expected_dict

    @pytest.mark.parametrize(
        "to_compare",
        [
            StatusEntry(
                time=datetime(2018, 9, 20),
                result=True,
                status=Status.WARNING,
                status_change=StatusChange.ERROR_RESOLVED,
            ),
            StatusEntry(
                time=datetime(2018, 9, 24),
                result=False,
                status=Status.WARNING,
                status_change=StatusChange.ERROR_RESOLVED,
            ),
            StatusEntry(
                time=datetime(2018, 9, 24),
                result=True,
                status=Status.OK,
                status_change=StatusChange.WARNING_RESOLVED,
            ),
        ],
    )
    def test_repr_is_unique(self, to_compare):
        orig_entry = StatusEntry(
            time=datetime(2018, 9, 24),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.ERROR_RESOLVED,
        )

        assert repr(orig_entry) != repr(to_compare)

    def test_string_representation(self):
        orig_entry = StatusEntry(
            time=datetime(2018, 9, 24),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.ERROR_RESOLVED,
        )

        expected_string = str(
            {
                "time": str(datetime(2018, 9, 24)),
                "result": True,
                "status": "WARNING",
                "status_change": "ERROR_RESOLVED",
            }
        )

        assert str(orig_entry) == expected_string
