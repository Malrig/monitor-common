import pytest

from datetime import datetime, date

from monitor_common.mongodb.status_entry import StatusEntry
from monitor_common.status import Status, StatusChange
from monitor_common.queries import day_summary, hour_summary, HourSummary


class TestHourSummary:
    def test_hour_range(self, mocked_db_clean):
        """Check that hours outside of 0-23 cause exceptions to be raised."""
        with pytest.raises(ValueError):
            hour_summary(date(2018, 9, 24), -1)

        with pytest.raises(ValueError):
            hour_summary(date(2018, 9, 24), 24)

    def test_entries_for_single_hour(self, mocked_db_clean):
        for minute in [0, 30]:
            StatusEntry(
                time=datetime(2018, 9, 24, 5, minute),
                result=True,
                status=Status.OK,
                status_change=StatusChange.NONE,
            ).save()

        for hour in range(0, 23):
            if hour == 5:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.OK
                )
            else:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.UNKNOWN
                )

    def test_date_vs_datetime(self, mocked_db_clean):
        for minute in [0, 30]:
            StatusEntry(
                time=datetime(2018, 9, 24, 5, minute),
                result=True,
                status=Status.OK,
                status_change=StatusChange.NONE,
            ).save()

        for hour in range(0, 23):
            assert hour_summary(date(2018, 9, 24), hour) == hour_summary(
                datetime(2018, 9, 24, 5, 4, 3), hour
            )

    def test_entries_with_different_statuses(self, mocked_db_clean):
        # Add some OK status entries
        for minute in [10, 30, 50]:
            StatusEntry(
                time=datetime(2018, 9, 24, 5, minute),
                result=True,
                status=Status.OK,
                status_change=StatusChange.NONE,
            ).save()

        # Add an ERROR status entry
        StatusEntry(
            time=datetime(2018, 9, 24, 5, 40),
            result=True,
            status=Status.ERROR,
            status_change=StatusChange.NONE,
        ).save()

        assert hour_summary(date(2018, 9, 24), 5) == HourSummary(
            date(2018, 9, 24), 5, Status.ERROR
        )

    def test_several_different_hours(self, mocked_db_clean):
        # Add a bunch of OK statuses to various hours
        for hour in [4, 5, 6, 7, 8]:
            for minute in [10, 30, 50]:
                StatusEntry(
                    time=datetime(2018, 9, 24, hour, minute),
                    result=True,
                    status=Status.OK,
                    status_change=StatusChange.NONE,
                ).save()

        # Add a WARNING status to hour 5
        StatusEntry(
            time=datetime(2018, 9, 24, 5, 40),
            result=True,
            status=Status.WARNING,
            status_change=StatusChange.NONE,
        ).save()

        # Add an ERROR status to hour 7
        StatusEntry(
            time=datetime(2018, 9, 24, 7, 40),
            result=True,
            status=Status.ERROR,
            status_change=StatusChange.NONE,
        ).save()

        for hour in range(0, 23):
            if hour in [4, 6, 8]:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.OK
                )
            elif hour == 5:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.WARNING
                )
            elif hour == 7:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.ERROR
                )
            else:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.UNKNOWN
                )


class TestDaySummary:
    def test_ok_day(self, mocked_db_clean):
        # Add a bunch of OK statuses to various hours
        for hour in [4, 5, 6, 7, 8]:
            for minute in [10, 30, 50]:
                StatusEntry(
                    time=datetime(2018, 9, 24, hour, minute),
                    result=True,
                    status=Status.OK,
                    status_change=StatusChange.NONE,
                ).save()

        summary = day_summary(date(2018, 9, 24))

        assert summary.overall_status == Status.OK
        assert summary.date == date(2018, 9, 24)
        for hour in range(0, 23):
            if hour in [4, 5, 6, 7, 8]:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.OK
                )
            else:
                assert hour_summary(date(2018, 9, 24), hour) == HourSummary(
                    date(2018, 9, 24), hour, Status.UNKNOWN
                )

    def test_date_vs_datetime(self, mocked_db_clean):
        # Add a bunch of OK statuses to various hours
        for hour in [4, 5, 6, 7, 8]:
            for minute in [10, 30, 50]:
                StatusEntry(
                    time=datetime(2018, 9, 24, hour, minute),
                    result=True,
                    status=Status.OK,
                    status_change=StatusChange.NONE,
                ).save()

        assert day_summary(date(2018, 9, 24)) == day_summary(
            datetime(2018, 9, 24, 5, 4, 3)
        )
