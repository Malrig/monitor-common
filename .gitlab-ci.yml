variables:
  PACKAGE_NAME: monitor_common

stages:
  # Run tests before lint so users get the most important answers first.
  - test
  - lint
  - deploy

.install_poetry: &install_poetry
  before_script:
    - pip install poetry
    - poetry config --local virtualenvs.in-project true
    - poetry install

.python_36: &python_36
  image: python:3.6
  cache:
    paths:
      - .venv
    key:
      files:
        - poetry.lock
        - myproject.toml
      prefix: python36

.python_37: &python_37
  image: python:3.7
  cache:
    paths:
      - .venv
    key:
      files:
        - poetry.lock
        - myproject.toml
      prefix: python37

.run_tests: &run_tests
  <<: *install_poetry
  stage: test
  services:
    - mongo:latest
  script:
    - poetry run pytest --cov=${PACKAGE_NAME} tests/ --cov-report=xml

testpy36:
  <<: *python_36
  <<: *run_tests

testpy37:
  <<: *python_37
  <<: *run_tests
  after_script:
    - bash <(curl -s https://codecov.io/bash) -t ${CODECOV_TOKEN}

.lint_common: &lint_common
  <<: *python_37
  <<: *install_poetry
  stage: lint

# Check type annotations for validity
mypy:
  <<: *lint_common
  script:
    - poetry run mypy -p ${PACKAGE_NAME} --ignore-missing-imports

# Basic code style enforcement
flake8:
  <<: *lint_common
  script:
    - poetry run flake8 ${PACKAGE_NAME}/ tests/

# Basic code style enforcement
black:
  <<: *lint_common
  script:
    - poetry run black . --check

# Basic docstring style enforcement
# This encourages following PEP257: https://www.python.org/dev/peps/pep-0257/
pydocstyle:
  <<: *lint_common
  allow_failure: true
  script:
    - poetry run pydocstyle ${PACKAGE_NAME} --ignore=D202,D203,D212

# Static analysis for known security problems in packages that this project depends on.
# It compares the dependent packages against https://github.com/pyupio/safety-db
safety_check:
  <<: *lint_common
  only:
    - master
  script:
    - poetry run safety check

.deploy_common: &deploy_common
  <<: *install_poetry
  <<: *python_37
  stage: deploy
  when: manual
  only:
    - master

deploy_production:
  <<: *deploy_common
  environment:
    name: production
  script:
    - poetry publish --username $PRODUCTION_USERNAME --password $PRODUCTION_PASSWORD --build

deploy_staging:
  <<: *deploy_common
  environment:
    name: staging
  script:
    - poetry config repositories.test_pypi https://test.pypi.org/legacy/
    - poetry publish -r test_pypi --username $STAGING_USERNAME --password $STAGING_PASSWORD --build
